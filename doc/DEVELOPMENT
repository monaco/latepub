
LatePub 0.1 - LaTeX to ePub conversion utilities (alpha release)
------------------------------------------

	Copyright (c) 2017 Monaco F. J. <monaco@usp.br>
	LaTePub is distributed under the terms of the GNU GPL.
	please refer to the file COPYING for further information.


LatePub project is hosted at http://www.gitlab.com/monaco/latepub


DEVELOPERS INSTRUCTIONS
-----------------------

You are very welcome to contribute to LatePub development.

If you are willing so, we thank you very much and introduce a few basic
project guidelines aimed at enforcing consistence, portability and easy
mantenance.

To that end

* bash scripts shall use /bin/sh (please refrain from bash-ism);
* binary code shall be written in strict POSIX and ISO C90 compliance;
* beware of the adopted versioning convention (see below).
* submit your contribution as a patch

HOW TO SUBMIT PATCHES
---------------------

To submit contributions, annotate your changes and additions directly on the
source code, next to the places you have edited.

Then, use 'diff -u' to produce a patch file by comparing your code to the
software at the official repository.

Open the patch file and add author and license information (see bellow for
suggestions). You will hold the author rights over the patch. We currently
accept patches distributed under the terms of GNU GPL vr3.

To send the patch,  contact the author(s) directly.


VERSION NUMBERING CONVENTION
----------------------------

This project is hosted in a Git repository and releases are identified by
tags. Version numbering is identified through the following logic scheme.

      major.minor[.stage].build 

Field build is sequentially incremented so as to reflect minimal changes like
bug fixes and improvements which do not break backward compatibility.

Field stage is meant to indicate the development stage: 0 for alpha, 1 for
beta, 2 for release candidate, 3 for production release.  This field is changed
at the developers' discretion according to their agreement on the project
maturity level along the respective development branch.  When stage is
incremented, field build is reset to zero.

Field minor is sequentially incremented so as to reflect minor changes
which break backward compatibility. When minor is changed, both stage and
build fields are reset to zero.

Field major is sequentially incremented upon project managers'
discretion to reflect significant milestones during the development
process.  When major is changed, minor, stage and build fields are reset
to zero.

For instance, a numbering sequence might be as follows

1.0.0.0       first alpha release of version 1.0
1.0.0.1	      backward compatible bug fixes on version 1.0 alpha
1.0.0.2	      backward compatible bug fixex on version 1.0 alpha
1.0.1.0	      first beta release of version 1.0
1.0.1.1	      backward compatible bug fixes on version 1.0 beta
1.0.1.2	      backward compatible bug fixes on version 1.0 beta
1.0.2.0	      first release candidate of version 1.0
1.0.2.1	      backward compatible bug fixes on version 1.0 release candidate
1.0.3.0	      first production release of version 1.0
1.0.3.1	      backward compatible bug fixes on version 1.0 (production)
1.0.3.1	      backward compatible bug fixes on version 1.0 (production)
1.1.0.0	      first alpha release of version 1.1


The version numbering logic scheme is actually implemented in its short
description format, according to the following rules.

* Field stage is represented by letters: 'a' replaces 0 for alpha, 'b'
  replaces 1 for beta, 'c' replaces 2  for releae candidate. For production
  releases the field stage is dropped such that the versioni number has only
  the three remaining fields major.minor.build.

  For instance, in the running exmple, the development sequence would read


1.0.a.0       first alpha release of version 1.0
1.0.a.1	      backward compatible bug fixes on version 1.0, alpha
1.0.a.2	      backward compatible bug fixex on version 1.0, alpha
1.0.b.0	      first beta release of version 1.0
1.0.b.1	      backward compatible bug fixes on version 1.0, beta
1.0.b.2	      backward compatible bug fixes on version 1.0, beta
1.0.c.0	      first release candidate of version 1.0
1.0.c.1	      backward compatible bug fixes on version 1.0, release candidate
1.0.0	      first production release of version 1.0
1.0.1	      backward compatible bug fixes on version 1.0, production
1.0.1	      backward compatible bug fixes on version 1.0, production
1.1.a.0	      first alpha release of version 1.1

COPYRIGHT AND ATTRIBUTION
---------------------------

When preparing a patch, as a suggestion, open the diff file and add a header
snippet like this

   Patch to <single line description of the patch's purpose>
   Copyright (c) Year, Full Name, <e-mail address>

   This patch file is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

Your patch, once accepted, will be added to the repository without
modifications. LatePub author(s) may apply your patch to the source code
and eventually edit the affected portions. In any case, you will retain
the copyright over your patch and your contribution will be properly
attributed.

Thank you very much!
LatePub author(s).



