
# LatePub - LaTeX to ePub conversion utilities

        Copyright (c) 2017 Monaco F. J.
        License GNU GPL

LatePub is a set of wrapper scripts around a several specialized free open
source tools which, in combination, may be used to produce a e-book in format
out from a book source edited in LaTeX format.  

The goal of LatePub toolchain is to allow this conversion without modification
of the source documents, so that these can be processed with standard LaTeX
system to produce aesthetically appealing PDF outputs, while still being
directly usable for generating reflowable e-book content.
